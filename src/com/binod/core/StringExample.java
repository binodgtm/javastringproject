package com.binod.core;

public class StringExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * //Write a Java program to get substring and concatenate two below
		 * strings to produce sample output in lowercase-
		 * 
		 * String 1: Java Exercises String 2: Doing String Assignment
		 * 
		 * Sample Output: java string assignment
		 */

		String str1 = "Java Exercise";

		String str2 = "Doing String Assignment";

		int l1 = str1.length();
		System.out.println(l1);

		int l2 = str2.length();
		System.out.println(l2);
		
		String substr1 = str1.substring(0, 4);
		
		String substr2 = str2.substring(6, l2);
		String strconcate = substr1 + " "+substr2;
		System.out.println(strconcate.toLowerCase());

		/*String str = str1 + " " + str2;
		System.out.println(str.toLowerCase());
		System.out.println(str.toUpperCase());*/

	}

}
