package com.binod.core;

public class StringConcept {

	public static void main(String[] args) {
		// two way of creating the string
		createStringByStringLiteral();
		createStringByKeyword();

	}

	private static void createStringByKeyword() {
		// TODO Auto-generated method stub
		String name1 = new String("David");
		String name2 = new String("David");
		System.out.println(name1.hashCode());
		System.out.println(name2.hashCode());

	}

	private static void createStringByStringLiteral() {
		// TODO Auto-generated method stub
		String name1 = "David";
		String name2 = "David";
		name1 = name1 + "Smith";

		System.out.println(name1.hashCode());
		System.out.println(name2.hashCode());
	}
}
