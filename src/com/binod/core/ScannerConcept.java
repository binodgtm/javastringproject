package com.binod.core;

import java.util.Scanner;

public class ScannerConcept {

	public static void main(String[] args) {
		int i = 0;
		
		while (i < 5) {
			Scanner scanner = new Scanner(System.in);
			
			System.out.println("Please enter your name");
			String name = scanner.next();
			
			System.out.println("Your name is: " + name);
			
			i++;
		}
	}

}
