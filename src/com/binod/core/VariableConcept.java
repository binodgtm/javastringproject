package com.binod.core;

public class VariableConcept {
	// non-Static field/instance variable/member variables
	String fname;
	String lname;
	int age;
	String email;
	// Static field/class variable 
	static String location;
	
	static{
		System.out.println("I am from a static block. I execute just one time.");
		location = "Irving";
	}
	
	public VariableConcept() {
		super();
		
		System.out.println("Hello From Default constructor");
		// TODO Auto-generated constructor stub
	}

	public VariableConcept(String fname, String lname, int age) {
		System.out.println("Hello From Overloaded - Constructor!");
		
		this.fname = fname;
		this.lname = lname;
		this.age = age;
	}
	
	
	public static void main(String[] args) {

		System.out.println(VariableConcept.location);
		// local variabe
		String email = "myemail@yahoo.com";

		// System.out.println(email);
		VariableConcept vc = new VariableConcept("Binod", "Gautam", 25);
		vc.showPersonalDetials();
		
		VariableConcept vc2 = new VariableConcept("Shyam", "Gautam", 57);
		vc2.location = "Dallas";
		vc2.showPersonalDetials();
		
		vc.showPersonalDetials();
		
		VariableConcept vc3 = new VariableConcept();
		vc3.showPersonalDetials();
	}

	private void showPersonalDetials() {
		// TODO Auto-generated method stub
		System.out.println("My frist name is :" + fname);
		System.out.println("last name is : " + lname);
		System.out.println("my age is :" + age);
		char[] email = null;
		System.out.println("email address is ;" + email);
		System.out.println("my location is : " + location);
		System.out.println("************");
	}

}
