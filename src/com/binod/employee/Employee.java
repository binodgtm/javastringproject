package com.binod.employee;

public class Employee {

	String ename;
	String esnumber;
	int eid;
	public static void main(String[] args) {
		
		Employee empl = new Employee("58915", "Binod", 101);
		
		System.out.println("Employee first name is: "+empl.ename);
		System.out.println("Employee ssn is: "+empl.esnumber);
		System.out.println("Employee id number is: "+empl.eid);
		empl.showEmployeeDetails();
		
	}
	
	public Employee(String ename, String esnumber, int eid) {
		super();
		this.ename = ename;
		this.esnumber = esnumber;
		this.eid = eid;
	}

	private void showEmployeeDetails() {
		System.out.println("Employee details has been provided");
		System.out.println("Employee name is : "+ename);
		System.out.println("Employee ssn is: "+esnumber);
		System.out.println("Employee id is: "+eid);
	}
}
